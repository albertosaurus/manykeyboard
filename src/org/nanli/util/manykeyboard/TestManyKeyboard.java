package org.nanli.util.manykeyboard;

import java.io.IOException;


/**
 * Created with IntelliJ IDEA.
 * User: nanli
 * Date: 5/24/13
 * Time: 7:31 PM
 * To change this template use File | Settings | File Templates.
 */
public class TestManyKeyboard implements ManyKeyEventListener
{

    @Override
    public void manyKeyEvent(ManyKeyEvent event)
    {
        if(!event.isActionKey())
        {
            System.err.println("Keyboard "+event.getKeyboardID() + " : "+event.getKeystring() + " " +event.getKeyType());
        }
    }

    //testing the main
    public static void main(String[] args) throws IOException, InterruptedException
    {
        TestManyKeyboard testManyKeyboard = new TestManyKeyboard();
        final ManyKeyboardManager keyboardManager = ManyKeyboardManager.INSTANCE;
        keyboardManager.addManyKeyEventListener(testManyKeyboard);

        new Thread()
        {
            public void run()
            {
                System.out.println("Initializing library");
                keyboardManager.initializeLibrary();
                System.out.println("Getting keyboard devices");
                keyboardManager.getKeyboardDevices();
                System.out.println("Opening keyboard devices");
                keyboardManager.openKeyboardDevices(false);
                System.out.println("Reading keyboard devices");
                keyboardManager.readKeyboardDevices();
                System.out.println("Closing keyboard devices");
                keyboardManager.closeKeyboardDevices();
                System.out.println("Done init thread");
            }
        }.start();


    }

}
